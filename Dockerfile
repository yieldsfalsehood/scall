FROM golang:latest AS build
WORKDIR /src
ADD . .
RUN ["/usr/bin/make", "install"]

FROM alpine:latest
COPY --from=build /go/bin/scall /usr/bin/
ENTRYPOINT ["/usr/bin/scall"]
