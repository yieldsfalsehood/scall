
SNAME = scall

BDIST = $(SNAME).tar.gz

export CGO_ENABLED ?= 0

all: $(SNAME)

.PHONY: build
build:
	go build -o . -i -v ./...
	touch -r . $(SNAME)

$(SNAME):
	$(MAKE) build

$(BDIST): bdist.mtree $(SNAME)
	bsdtar -czf $@ @$<

.PHONY: bdist
bdist: $(BDIST)

.PHONY: format-source
format-source:
	go fmt ./...

.PHONY: vet
vet:
	go vet ./...

.PHONY: check
check: $(SNAME)
	@find scripts/check.d \
	  -executable \
	  -type f \
	  -name '*.sh' \
	  -exec scripts/check '{}' \;

.PHONY: install
install:
	go install -i -v ./...

.PHONY: docker
docker:
	git archive HEAD | gzip | docker build -t $(SNAME) -

.PHONY: clean
clean:
	rm -f $(SNAME)
	rm -f $(BDIST)
