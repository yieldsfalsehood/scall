
# scall

cli wrappers over your favorite syscalls

## Overview

Each of these tools makes a system call that modifies the current
process state in some way then `execv`s another command inheriting
that new state. `close`, `dup2`, and `pipe` are used to create and
close file descriptors. `fork` and `sequence` are used to orchestrate
processes.

Altogether these act as a sort of shell replacement with some type
safety since they let us do file redirection and multiprocessing by
describing those programs as structured data rather than embedding
them in shell code. That facilitates code generation, so we can avoid
non-atomic forms that are difficult to build dynamically:

```sh
$ sh -c "cmd && cmd2"
```

Instead we can formalize some structure around those command
descriptions. This is less human friendly, but my computer seems to
like it:

```sh
$ scall sequence - cmd1 - cmd2 -
```

Of course our current shell still parses that into individual tokens,
but we might imagine storing that elsewhere to be `execv`'d by some
higher level tooling, which can take responsibility for how to embed
those commands:

```yaml
argv: ["scall", "sequence", "-", "cmd1", "-", "cmd2", "-"]
```

See [execvm](https://gitlab.com/yields.falsehood/execvm) for code
generation.

## Commands

See `scripts/check.d` for example usages.

### close

```sh
$ scall close fd delimiter argv delimiter
```

call `close(fd)` then `execv` `argv`

### dup2

```sh
$ scall dup2 fd1 fd2 delimeter argv delimeter
```

call `dup2(fd1, fd2)` then `execv` `argv`

### fork

```sh
$ scall fork delimeter argv1 delimeter argv2 delimiter
```

call `fork`

`execv` `argv1` in the forked process

`execv` `argv2` in the current process

### pipe

```sh
$ scall pipe delimeter argv delimeter
```

call `pipe` then `execv` `argv`

### sequence

```sh
$ scall sequence delimeter argv1 delimeter argv2 delimiter
```

call `fork`

`execv` `argv1` in the forked process

call `waitpid` in the current process

`execv` `argv2` in the current process

## cmdlang

```
fd:
  "fd" handle
  "open" "read" path
  "open" "write" path
  "vsock" "listen" addr
  "vsock" "connect" addr
```

```
device:
  "dev" path
  "block" "gpt" uuid
  "block" "gpt-partition" uuid
```

## Development

`make` and `go` are needed to build. `bash` is needed to run
checks. `libarchive`'s bsdtar is needed for creating the
bdist. `docker` and `git` are needed for build the docker image.

```sh
$ make
$ make check
```
