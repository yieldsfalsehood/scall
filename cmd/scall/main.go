package main

import (
	"os"
)

import (
	"gitlab.com/yieldsfalsehood/scall/internal/scall"
)

func main() {
	scall.Main(os.Args)
}
