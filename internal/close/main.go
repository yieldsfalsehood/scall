package close

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/parsefd"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
	"gitlab.com/yieldsfalsehood/scall/internal/utils"
)

var (
	logger logging.Logger
)

func parseArgs(args []string) (int, *syscall.ExecVE, error) {

	if fd, tail, err := parsefd.Parse1(args); err != nil {
		return -1, nil, err
	} else {
		execution, err := utils.ParseExecution(tail)
		return fd, execution, err
	}

}

func init() {
	logger = logging.NewLogger("scall/close:", logging.Lshortfile|logging.LstdFlags)
}

func Main(args []string) error {

	// close fd - command -

	if fd, execution, err := parseArgs(args); err != nil {
		return err
	} else {

		err := syscall.Close(fd)

		if err != nil {
			return err
		}

		return execution.Exec()

	}

}
