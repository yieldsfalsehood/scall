package cmdlang

import (
	"errors"
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
)

var (
	logger logging.Logger
)

type State struct {
	Input []string
}

func (state State) Next() ([]string, State, error) {

	if len(state.Input) < 2 {
		return nil, State{}, errors.New(fmt.Sprintf("no more input: %v", state.Input))
	}

	head, tail := state.Input[0], state.Input[1:]

	for i, v := range tail {

		if v == head {

			logger.Printf(logging.TraceLevel,
				"found '%s' at %d in %v",
				head, i, tail,
			)

			return tail[:i], State{tail[i:]}, nil

		}

	}

	return nil, State{}, errors.New(fmt.Sprintf("could not find %v in %v", head, tail))

}

func Parse1(input []string) ([]string, error) {

	state := State{input}

	argv, _, err := state.Next()

	return argv, err

}

func Parse2(input []string) ([]string, []string, error) {

	state := State{input}

	argv1, next, err1 := state.Next()

	if err1 != nil {
		return nil, nil, fmt.Errorf("while parsing first: %v", err1)
	}

	argv2, _, err2 := next.Next()

	if err2 != nil {
		return nil, nil, fmt.Errorf("while parsing second: %v", err2)
	}

	return argv1, argv2, nil

}

func init() {
	logger = logging.NewLogger("scall/cmdlang:", logging.Lshortfile|logging.LstdFlags)
}
