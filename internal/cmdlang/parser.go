package cmdlang

import (
	"errors"
)

func ConsumeOne(args []string) (string, []string, error) {

	if len(args) < 1 {
		return "", args, errors.New("not enough args")
	}

	return args[0], args[1:], nil

}
