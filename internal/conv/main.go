package conv

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"strconv"
)

var (
	logger logging.Logger
)

func init() {
	logger = logging.NewLogger("scall/conv:", logging.Lshortfile|logging.LstdFlags)
}

func ParseUint32(s string) (uint32, error) {
	ui64, err := strconv.ParseUint(s, 0, 32)
	logger.Printf(logging.TraceLevel, "parse %v -> %v", s, ui64)
	return uint32(ui64), err
}

/*
dev = makedev(xatoul_range(argv[2], 0, major(UINT_MAX)),
              xatoul_range(argv[3], 0, minor(UINT_MAX)));
*/
