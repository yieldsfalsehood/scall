package dup2

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/parsefd"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
	"gitlab.com/yieldsfalsehood/scall/internal/utils"
)

var (
	logger logging.Logger
)

func parseRedirection(fd1 int, fd2 int, args []string) (*syscall.Redirection, error) {

	execution, err := utils.ParseExecution(args)

	if err != nil {
		return nil, err
	}

	return &syscall.Redirection{
		OldFd:     fd1,
		NewFd:     fd2,
		Execution: execution,
	}, nil

}

func parseArgs(args []string) (*syscall.Redirection, error) {

	fd1, fd2, tail, err := parsefd.Parse2(args)

	if err != nil {
		return nil, err
	}

	return parseRedirection(fd1, fd2, tail)

}

func init() {
	logger = logging.NewLogger("scall/dup2:", logging.Lshortfile|logging.LstdFlags)
}

func Main(args []string) error {

	// dup2 fd1 fd2 - command -

	redirection, err := parseArgs(args)

	if err != nil {
		logger.Fatal(err)
	}

	return redirection.Exec()

}
