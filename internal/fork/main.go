package fork

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
	"log"
	"os"
)

var (
	logger logging.Logger
)

func parse(input []string) ([]string, []string) {

	command1, command2, err := cmdlang.Parse2(input)

	if err != nil {
		log.Fatal(fmt.Errorf("while parsing input: %v", err))
	}

	return command1, command2

}

func init() {
	logger = logging.NewLogger("scall/fork:", logging.Lshortfile|logging.LstdFlags)
}

func Main(args []string) error {

	// fork - c1 - c2 -

	if len(args) < 5 {
		fmt.Fprintln(os.Stderr, "usage")
		os.Exit(1)
	}

	argv1, argv2 := parse(args)
	environ := os.Environ()

	execution1 := syscall.ExecVE{
		Argv: argv1,
		Envp: environ,
	}
	execution2 := syscall.ExecVE{
		Argv: argv2,
		Envp: environ,
	}

	logger.Print(logging.DebugLevel, "fork")

	_, err := execution1.ForkExec()

	if err != nil {
		log.Fatal(err)
	}

	logger.Print(logging.DebugLevel, "pivot!")

	return execution2.Exec()

}
