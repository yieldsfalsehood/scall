package gpt

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"golang.org/x/sys/unix"
	"os"
)

// Disk is a reference to a single disk block device or image that has been Create() or Open()
type Disk struct {
	File              *os.File
	LogicalBlocksize  int64
	PhysicalBlocksize int64
}

// getSectorSizes get the logical and physical sector sizes for a block device
func getSectorSizes(file *os.File) (int64, int64, error) {
	/*
		ioctl(fd, BLKBSZGET, &physicalsectsize);
	*/
	fd := file.Fd()
	logicalSectorSize, err := unix.IoctlGetInt(int(fd), blksszGet)
	if err != nil {
		return 0, 0, fmt.Errorf("Unable to get device logical sector size: %v", err)
	}
	physicalSectorSize, err := unix.IoctlGetInt(int(fd), blkbszGet)
	if err != nil {
		return 0, 0, fmt.Errorf("Unable to get device physical sector size: %v", err)
	}
	return int64(logicalSectorSize), int64(physicalSectorSize), nil
}

func Open(path string) (*Disk, error) {

	if file, err := os.OpenFile(path, os.O_RDONLY, 0600); err != nil {
		return nil, fmt.Errorf("Could not open device %s: %v", path, err)
	} else {
		if lblksize, pblksize, err := getSectorSizes(file); err != nil {
			logger.Printf(logging.TraceLevel, "failed to get sector size for %v: %v", path, err)
			return nil, err
		} else {
			logger.Printf(logging.TraceLevel, "open(): logical block size %d, physical block size %d", lblksize, pblksize)
			ret := &Disk{
				File:              file,
				LogicalBlocksize:  lblksize,
				PhysicalBlocksize: pblksize,
			}
			return ret, nil
		}
	}
}

// Read read a partition table from a disk
// must be passed the util.File from which to read, and the logical and physical block sizes
//
// if successful, returns a gpt.Table struct
// returns errors if fails at any stage reading the disk or processing the bytes on disk as a GPT
func (disk *Disk) Read(logicalBlockSize, physicalBlockSize int) (*Table, error) {
	// read the data off of the disk
	b := make([]byte, gptSize+logicalBlockSize*2, gptSize+logicalBlockSize*2)
	read, err := disk.File.ReadAt(b, 0)
	if err != nil {
		return nil, fmt.Errorf("Error reading GPT from file: %v", err)
	}
	if read != len(b) {
		return nil, fmt.Errorf("Read only %d bytes of GPT from file instead of expected %d", read, len(b))
	}
	return tableFromBytes(b, logicalBlockSize, physicalBlockSize)
}
