package gpt

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
)

var logger logging.Logger

const (
	defaultBlocksize, firstblock int = 512, 2048
	blksszGet                        = 0x1268
	blkbszGet                        = 0x80081270
)

func init() {
	logger = logging.NewLogger("scall/syscall:", logging.Lshortfile|logging.LstdFlags)
}
