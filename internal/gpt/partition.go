package gpt

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/uuid"
	"strings"
	"unicode/utf16"
)

// PartitionEntrySize fixed size of a GPT partition entry
const PartitionEntrySize = 128

// Partition represents the structure of a single partition on the disk
type Partition struct {
	Start              uint64 // start sector for the partition
	End                uint64 // end sector for the partition
	Size               uint64 // size of the partition in bytes
	Type               Type   // parttype for the partition
	Name               string // name for the partition
	GUID               string // partition GUID, can be left blank to auto-generate
	Attributes         uint64 // Attributes flags
	logicalSectorSize  int
	physicalSectorSize int
}

// FromBytes create a partition entry from bytes
func partitionFromBytes(b []byte, logicalSectorSize, physicalSectorSize int) (*Partition, error) {
	if len(b) != PartitionEntrySize {
		return nil, fmt.Errorf("Data for partition was %d bytes instead of expected %d", len(b), PartitionEntrySize)
	}
	// is it all zeroes?
	typeGUID, err := uuid.FromBytes(bytesToUUIDBytes(b[0:16]))
	if err != nil {
		return nil, fmt.Errorf("unable to read partition type GUID: %v", err)
	}
	typeString := typeGUID.String()
	uuid, err := uuid.FromBytes(bytesToUUIDBytes(b[16:32]))
	if err != nil {
		return nil, fmt.Errorf("unable to read partition identifier GUID: %v", err)
	}
	firstLBA := binary.LittleEndian.Uint64(b[32:40])
	lastLBA := binary.LittleEndian.Uint64(b[40:48])
	attribs := binary.LittleEndian.Uint64(b[48:56])

	// get the partition name
	nameb := b[56:]
	u := make([]uint16, 0, 72)
	for i := 0; i < len(nameb); i += 2 {
		// strip any 0s off of the end
		entry := binary.LittleEndian.Uint16(nameb[i : i+2])
		if entry == 0 {
			break
		}
		u = append(u, entry)
	}
	r := utf16.Decode(u)
	name := string(r)

	return &Partition{
		Start:              firstLBA,
		End:                lastLBA,
		Name:               name,
		GUID:               strings.ToUpper(uuid.String()),
		Attributes:         attribs,
		Type:               Type(strings.ToUpper(typeString)),
		logicalSectorSize:  logicalSectorSize,
		physicalSectorSize: physicalSectorSize,
	}, nil
}
