package gpt

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"hash/crc32"
	"strings"
	// "gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/uuid"
)

// Table represents a partition table to be applied to a disk or read from a disk
type Table struct {
	Partitions         []*Partition // slice of Partition
	LogicalSectorSize  int          // logical size of a sector
	PhysicalSectorSize int          // physical size of the sector
	GUID               string       // disk GUID, can be left blank to auto-generate
	ProtectiveMBR      bool         // whether or not a protective MBR is in place
	partitionArraySize int          // how many entries are in the partition array size
	partitionEntrySize uint32       // size of the partition entry in the table, usually 128 bytes
	primaryHeader      uint64       // LBA of primary header, always 1
	secondaryHeader    uint64       // LBA of secondary header, always last sectors on disk
	firstDataSector    uint64       // LBA of first data sector
	lastDataSector     uint64       // LBA of last data sector
	initialized        bool
}

// gptSize max potential size for partition array reserved 16384
const (
	gptSize                  = 128 * 128
	mbrPartitionEntriesStart = 446
	mbrPartitionEntriesCount = 4
	mbrpartitionEntrySize    = 16
	// just defaults
	physicalSectorSize = 512
	logicalSectorSize  = 512
)

func getEfiSignature() []byte {
	return []byte{0x45, 0x46, 0x49, 0x20, 0x50, 0x41, 0x52, 0x54}
}
func getEfiRevision() []byte {
	return []byte{0x00, 0x00, 0x01, 0x00}
}
func getEfiHeaderSize() []byte {
	return []byte{0x5c, 0x00, 0x00, 0x00}
}
func getEfiZeroes() []byte {
	return []byte{0x00, 0x00, 0x00, 0x00}
}
func getMbrSignature() []byte {
	return []byte{0x55, 0xaa}
}

// check if a byte slice is all zeroes
func zeroMatch(b []byte) bool {
	if len(b) < 1 {
		return true
	}
	for _, val := range b {
		if val != 0 {
			return false
		}
	}
	return true
}

// tableFromBytes read a partition table from a byte slice
func tableFromBytes(b []byte, logicalBlockSize, physicalBlockSize int) (*Table, error) {

	// minimum size - gpt entries + header + LBA0 for (protective) MBR
	minSize := gptSize + logicalBlockSize*2
	if len(b) < minSize {
		return nil, fmt.Errorf("Data for partition was %d bytes instead of expected minimum %d", len(b), minSize)
	}

	// GPT starts at LBA1
	gpt := b[logicalBlockSize:]
	// start with fixed headers
	efiSignature := gpt[0:8]
	efiRevision := gpt[8:12]
	efiHeaderSize := gpt[12:16]
	efiHeaderCrcBytes := append(make([]byte, 0, 4), gpt[16:20]...)
	efiHeaderCrc := binary.LittleEndian.Uint32(efiHeaderCrcBytes)
	efiZeroes := gpt[20:24]
	primaryHeader := binary.LittleEndian.Uint64(gpt[24:32])
	secondaryHeader := binary.LittleEndian.Uint64(gpt[32:40])
	firstDataSector := binary.LittleEndian.Uint64(gpt[40:48])
	lastDataSector := binary.LittleEndian.Uint64(gpt[48:56])
	diskGUID, err := uuid.FromBytes(bytesToUUIDBytes(gpt[56:72]))
	if err != nil {
		return nil, fmt.Errorf("Unable to read guid from disk: %v", err)
	}
	partitionEntryFirstLBA := binary.LittleEndian.Uint64(gpt[72:80])
	partitionEntryCount := binary.LittleEndian.Uint32(gpt[80:84])
	partitionEntrySize := binary.LittleEndian.Uint32(gpt[84:88])
	partitionEntryChecksum := binary.LittleEndian.Uint32(gpt[88:92])

	// once we have the header CRC, zero it out
	copy(gpt[16:20], []byte{0x00, 0x00, 0x00, 0x00})
	if bytes.Compare(efiSignature, getEfiSignature()) != 0 {
		return nil, fmt.Errorf("Invalid EFI Signature %v", efiSignature)
	}
	if bytes.Compare(efiRevision, getEfiRevision()) != 0 {
		return nil, fmt.Errorf("Invalid EFI Revision %v", efiRevision)
	}
	if bytes.Compare(efiHeaderSize, getEfiHeaderSize()) != 0 {
		return nil, fmt.Errorf("Invalid EFI Header size %v", efiHeaderSize)
	}
	if bytes.Compare(efiZeroes, getEfiZeroes()) != 0 {
		return nil, fmt.Errorf("Invalid EFI Header, expected zeroes, got %v", efiZeroes)
	}
	// get the checksum
	checksum := crc32.ChecksumIEEE(gpt[0:92])
	if efiHeaderCrc != checksum {
		return nil, fmt.Errorf("Invalid EFI Header Checksum, expected %v, got %v", checksum, efiHeaderCrc)
	}

	// now for partitions
	partArrayStart := partitionEntryFirstLBA * uint64(logicalBlockSize)
	partArrayEnd := partArrayStart + uint64(partitionEntryCount*partitionEntrySize)
	bpart := b[partArrayStart:partArrayEnd]
	// we need a CRC/zlib of the partition entries, so we do those first, then append the bytes
	checksum = crc32.ChecksumIEEE(bpart)
	if partitionEntryChecksum != checksum {
		return nil, fmt.Errorf("Invalid EFI Partition Entry Checksum, expected %v, got %v", checksum, partitionEntryChecksum)
	}

	parts := make([]*Partition, 0, partitionEntryCount)
	count := int(partitionEntryCount)
	for i := 0; i < count; i++ {
		// write the primary partition entry
		start := i * int(partitionEntrySize)
		end := start + int(partitionEntrySize)
		// skip all 0s
		p, err := partitionFromBytes(bpart[start:end], logicalBlockSize, physicalBlockSize)
		if err != nil {
			return nil, fmt.Errorf("Error reading partition entry %d: %v", i, err)
		}
		// augment partition information
		p.Size = (p.End - p.Start + 1) * uint64(logicalBlockSize)
		parts = append(parts, p)
	}

	table := Table{
		LogicalSectorSize:  logicalBlockSize,
		PhysicalSectorSize: physicalBlockSize,
		partitionEntrySize: partitionEntrySize,
		primaryHeader:      primaryHeader,
		secondaryHeader:    secondaryHeader,
		firstDataSector:    firstDataSector,
		lastDataSector:     lastDataSector,
		partitionArraySize: int(partitionEntryCount),
		GUID:               strings.ToUpper(diskGUID.String()),
		Partitions:         parts,
	}

	// logger.Printf(logging.DebugLevel, "read(): %v", table)

	return &table, nil
}
