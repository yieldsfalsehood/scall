package logging

import (
	"log"
	"os"
	"strconv"
)

type Level int8

const (
	Ldate         = log.Ldate
	Ltime         = log.Ltime
	Lmicroseconds = log.Lmicroseconds
	Llongfile     = log.Llongfile
	Lshortfile    = log.Lshortfile
	LUTC          = log.LUTC
	Lmsgprefix    = log.Lmsgprefix
	LstdFlags     = log.LstdFlags
)

const (
	// DebugLevel logs are typically voluminous, and are usually disabled in
	// production.
	TraceLevel Level = iota - 3
	Debug2Level
	DebugLevel
	// InfoLevel is the default logging priority.
	InfoLevel
	// WarnLevel logs are more important than Info, but don't need individual
	// human review.
	WarnLevel
	// ErrorLevel logs are high-priority. If an application is running smoothly,
	// it shouldn't generate any error-level logs.
	ErrorLevel
	// DPanicLevel logs are particularly important errors. In development the
	// logger panics after writing the message.
	DPanicLevel
	// PanicLevel logs a message, then panics.
	PanicLevel
	// FatalLevel logs a message, then calls os.Exit(1).
	FatalLevel
)

type Logger struct {
	level  Level
	logger *log.Logger
}

func (logger Logger) Fatal(v ...interface{}) {
	logger.logger.Fatal(v...)
}

func (logger Logger) Fatalf(format string, v ...interface{}) {
	logger.logger.Fatalf(format, v...)
}

func (logger Logger) Print(level Level, v ...interface{}) {
	if level >= logger.level {
		logger.logger.Print(v...)
	}
}

func (logger Logger) Printf(level Level, format string, v ...interface{}) {
	if level >= logger.level {
		logger.logger.Printf(format, v...)
	}
}

func getLogLevel() Level {

	value, ok := os.LookupEnv("SCALL_LOGLEVEL")

	if ok {

		level, err := strconv.Atoi(value)

		if err != nil {
			log.Fatal(err)
		}

		return Level(level)

	}

	return InfoLevel

}

func NewLogger(prefix string, flag int) Logger {

	return Logger{
		level:  getLogLevel(),
		logger: log.New(os.Stderr, prefix, flag),
	}

}
