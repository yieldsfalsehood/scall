package mknod

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/parsedev"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
)

var (
	logger logging.Logger
)

func parseTarget(args []string) (string, []string, error) {

	// length check

	return args[0], args[1:], nil

}

func parseArgs(args []string) (string, parsedev.Device, error) {
	if path, tail, err := parseTarget(args); err != nil {
		return path, nil, err
	} else {
		device, _, err := parsedev.Parse1(tail)
		return path, device, err
	}
}

func init() {
	logger = logging.NewLogger("scall/mknod:", logging.Lshortfile|logging.LstdFlags)
}

func Main(args []string) error {

	// mknod <path> device

	if path, device, err := parseArgs(args); err != nil {
		return err
	} else {
		mode, dev, err := device.Dev()
		if err != nil {
			return err
		}
		return syscall.Mknod(path, mode, dev)
	}

}
