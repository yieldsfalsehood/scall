package parsedev

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/gpt"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/uuid"
	"strings"
)

var (
	BlockConstructors = map[string]DeviceConstructor{
		"gpt":           gptConstructor,
		"gpt-partition": gptPartitionConstructor,
	}
)

type GptDiskProbe struct {
	id uuid.UUID
}

func (probe GptDiskProbe) probe(path string) bool {

	needle := strings.ToUpper(probe.id.String())

	logger.Printf(logging.DebugLevel, "probing %v", path)
	if disk, err := gpt.Open(path); err != nil {
		// logger.Printf(logging.DebugLevel, "skipping %v: %v", path, err)
		return false
	} else {
		table, err := disk.Read(int(disk.LogicalBlocksize), int(disk.PhysicalBlocksize))
		if err != nil {
			return false
		}
		return table.GUID == needle
	}

}

func parseUUID(args []string) (uuid.UUID, []string, error) {

	if id, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return uuid.UUID{Bytes: nil}, args, err
	} else {
		decoded, err := hex.DecodeString(id)
		if err != nil {
			return uuid.UUID{Bytes: nil}, args, err
		}
		return uuid.UUID{Bytes: decoded}, tail, nil
	}

}

func gptConstructor(args []string) (Device, []string, error) {
	if id, tail, err := parseUUID(args); err != nil {
		return nil, args, err
	} else {
		probe := GptDiskProbe{id}
		device, err := probeForDevice("block", probe.probe)
		if err != nil {
			return nil, args, err
		}
		return device, tail, nil
	}
}

func gptPartitionConstructor([]string) (Device, []string, error) {
	return nil, nil, nil
}

func blockConstructor(args []string) (Device, []string, error) {

	if name, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return nil, args, err
	} else {

		constructor, exists := BlockConstructors[name]

		if !exists {
			return nil, tail, fmt.Errorf("unrecognized block device constructor %v", name)
		}

		return constructor(tail)

	}

}
