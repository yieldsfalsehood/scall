package parsedev

import (
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
)

type CloneDevice struct {
	path string
}

func (device CloneDevice) Dev() (uint32, int, error) {

	stat, err := syscall.Stat(device.path)

	if err != nil {
		return 0, 0, err
	}

	// from uint32, uint64
	return stat.Mode, int(stat.Rdev), nil

}

func cloneConstructor(args []string) (Device, []string, error) {

	if path, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return nil, args, err
	} else {
		return CloneDevice{path}, tail, nil
	}

}
