package parsedev

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
)

type Device interface {
	Dev() (uint32, int, error)
}

type DeviceConstructor = func([]string) (Device, []string, error)

var (
	logger             logging.Logger
	DeviceConstructors = map[string]DeviceConstructor{
		"dev":   cloneConstructor,
		"block": blockConstructor,
	}
)

func init() {
	logger = logging.NewLogger("scall/parsedev:", logging.Lshortfile|logging.LstdFlags)
}

func Parse1(args []string) (Device, []string, error) {

	if name, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return nil, args, err
	} else {

		constructor, exists := DeviceConstructors[name]

		if !exists {
			return nil, tail, fmt.Errorf("unrecognized device constructor %v", name)
		}

		return constructor(tail)

	}

}
