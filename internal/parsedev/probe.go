package parsedev

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"os"
)

type Probe = func(string) bool

func findClassDevices(class string) ([]string, error) {

	path := fmt.Sprintf("/sys/class/%s", class)

	if dir, err := os.Open(path); err != nil {
		return nil, err
	} else {
		return dir.Readdirnames(0)
	}

}

func probeForDevice(class string, probe Probe) (Device, error) {
	if devs, err := findClassDevices(class); err != nil {
		return nil, err
	} else {
		logger.Printf(logging.DebugLevel, "probing %v devices: %v", class, devs)
		for _, dev := range devs {
			path := fmt.Sprintf("/dev/%s", dev)
			if probe(path) {
				logger.Printf(logging.DebugLevel, "found match: %v", dev)
				return CloneDevice{path}, nil
			}
		}
	}
	return nil, fmt.Errorf("device not found")
}
