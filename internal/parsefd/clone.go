package parsefd

import (
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"strconv"
)

func cloneConstructor(args []string) (int, []string, error) {

	if handle, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return -1, args, err
	} else {

		fd, err := strconv.Atoi(handle)

		logger.Printf(logging.TraceLevel, "fd: %v", fd)

		return fd, tail, err

	}

}
