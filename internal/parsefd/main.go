package parsefd

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
)

type FdConstructor = func([]string) (int, []string, error)

var (
	FdConstructors = map[string]FdConstructor{
		"fd":    cloneConstructor,
		"open":  openConstructor,
		"vsock": vsockConstructor,
	}
	logger logging.Logger
)

func init() {
	logger = logging.NewLogger("scall/parsefd:", logging.Lshortfile|logging.LstdFlags)
}

func Parse1(args []string) (int, []string, error) {

	if name, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return -1, args, err
	} else {

		constructor, exists := FdConstructors[name]

		if !exists {
			return -1, tail, fmt.Errorf("unrecognized fd constructor %v", name)
		}

		return constructor(tail)

	}

}

func Parse2(args []string) (int, int, []string, error) {

	if fd1, tail1, err := Parse1(args); err != nil {
		return fd1, -1, tail1, err
	} else {
		fd2, tail2, err := Parse1(tail1)
		return fd1, fd2, tail2, err
	}

}
