package parsefd

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
	"os"
)

var (
	methodFlags = map[string]int{
		// set O_NOCTTY, ditch rw
		"read":  os.O_RDONLY | syscall.O_NOCTTY,
		"write": os.O_WRONLY | syscall.O_NOCTTY | os.O_CREATE | os.O_TRUNC,
	}
)

func open(flag int, args []string) (int, []string, error) {

	if name, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return -1, args, err
	} else {

		fd, err := syscall.Open(name, flag, 0644)

		return fd, tail, err

	}

}

func openConstructor(args []string) (int, []string, error) {

	if method, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return -1, args, err
	} else {

		flag, exists := methodFlags[method]

		if !exists {
			return -1, tail, fmt.Errorf("unrecognized mode %v", method)
		}

		return open(flag, tail)

	}

}
