package parsefd

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/conv"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"golang.org/x/sys/unix"
	"strings"
)

type VsockOpener = func(int, *unix.SockaddrVM) (int, error)

var (
	vsockOpeners = map[string]VsockOpener{
		"listen":  listenVsock,
		"connect": connectVsock,
	}
	CIDS = map[string]uint32{
		"ANY":        unix.VMADDR_CID_ANY,
		"HOST":       unix.VMADDR_CID_HOST,
		"HYPERVISOR": unix.VMADDR_CID_HYPERVISOR,
		"LOCAL":      unix.VMADDR_CID_LOCAL,
	}
	PORTS = map[string]uint32{
		"ANY": unix.VMADDR_PORT_ANY,
	}
)

func sas(sockaddr *unix.SockaddrVM) string {
	return fmt.Sprintf("vsock(%v:%v)", sockaddr.CID, sockaddr.Port)
}

func parseSockaddrPart(special map[string]uint32, part string) (uint32, error) {

	value, exists := special[strings.ToUpper(part)]

	if !exists {
		return conv.ParseUint32(part)
	}

	return value, nil

}

func coalesceErrors(errs ...error) error {
	for _, err := range errs {
		if err != nil {
			return err
		}
	}
	return nil
}

func parseSockaddrParts(parts []string) (*unix.SockaddrVM, error) {

	cid, err1 := parseSockaddrPart(CIDS, parts[0])
	port, err2 := parseSockaddrPart(PORTS, parts[1])

	if err1 != nil || err2 != nil {
		return nil, coalesceErrors(err1, err2)
	}

	return &unix.SockaddrVM{
		CID:  cid,
		Port: port,
	}, nil

}

func parseSockaddr(source string) (*unix.SockaddrVM, error) {

	parts := strings.SplitN(source, ":", 2)

	if len(parts) < 2 {
		return nil, fmt.Errorf("vsock addr should be host:port, got '%v'", source)
	}

	return parseSockaddrParts(parts)

}

func listenVsock(fd int, sockaddr *unix.SockaddrVM) (int, error) {

	if err := unix.Bind(fd, sockaddr); err != nil {
		return -1, err
	}

	if err := unix.Listen(fd, 0); err != nil {
		return -1, err
	}

	if sa, err := unix.Getsockname(fd); err != nil {
		return -1, err
	} else {
		server := sa.(*unix.SockaddrVM)
		logger.Printf(logging.DebugLevel, "listening at %v", sas(server))
	}

	nfd, sa, err := unix.Accept(fd)

	if err != nil {
		client := sa.(*unix.SockaddrVM)
		logger.Printf(logging.DebugLevel, "received connection from %v", sas(client))
	}

	return nfd, err

}

func connectVsock(fd int, sockaddr *unix.SockaddrVM) (int, error) {
	logger.Printf(logging.DebugLevel, "connecting to %v", sas(sockaddr))
	return fd, unix.Connect(fd, sockaddr)
}

func dispatchOpenVsock(opener VsockOpener, fd int, sockaddr *unix.SockaddrVM, args []string) (int, []string, error) {

	nfd, err := opener(fd, sockaddr)

	return nfd, args, err

}
func openSocket(opener VsockOpener, source string, args []string) (int, []string, error) {

	if sockaddr, err := parseSockaddr(source); err != nil {
		return -1, args, err
	} else {

		fd, err := unix.Socket(unix.AF_VSOCK, unix.SOCK_STREAM, 0)

		if err != nil {
			return -1, args, err
		}

		return dispatchOpenVsock(opener, fd, sockaddr, args)
	}

}

func openVsock(opener VsockOpener, args []string) (int, []string, error) {

	if source, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return -1, args, err
	} else {
		return openSocket(opener, source, tail)
	}

}

func vsockConstructor(args []string) (int, []string, error) {

	if mode, tail, err := cmdlang.ConsumeOne(args); err != nil {
		return -1, args, err
	} else {

		opener, exists := vsockOpeners[mode]

		if !exists {
			return -1, tail, fmt.Errorf("unrecognized vsock mode %v", mode)
		}

		return openVsock(opener, tail)

	}

}
