package pipe

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
	"gitlab.com/yieldsfalsehood/scall/internal/utils"
)

var (
	logger logging.Logger
)

func init() {
	logger = logging.NewLogger("scall/pipe:", logging.Lshortfile|logging.LstdFlags)
}

func Main(args []string) error {

	if execution, err := utils.ParseExecution(args); err != nil {
		return err
	} else {

		fds, err := syscall.Pipe()

		logger.Printf(logging.DebugLevel, "got fds %v", fds)

		if err != nil {
			return err
		}

		return execution.Exec()

	}

}
