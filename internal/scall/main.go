package scall

import (
	"fmt"
	"gitlab.com/yieldsfalsehood/scall/internal/close"
	"gitlab.com/yieldsfalsehood/scall/internal/dup2"
	"gitlab.com/yieldsfalsehood/scall/internal/fork"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"gitlab.com/yieldsfalsehood/scall/internal/mknod"
	"gitlab.com/yieldsfalsehood/scall/internal/pipe"
	"gitlab.com/yieldsfalsehood/scall/internal/sequence"
	"path"
	"strings"
)

type commandHandler = func([]string) error

var (
	logger          logging.Logger
	commandHandlers = map[string]commandHandler{
		"close":    close.Main,
		"dup2":     dup2.Main,
		"fork":     fork.Main,
		"mknod":    mknod.Main,
		"pipe":     pipe.Main,
		"sequence": sequence.Main,
	}
)

func parseHandler(input string) (commandHandler, error) {

	handler, exists := commandHandlers[strings.ToLower(input)]

	if !exists {
		return nil, fmt.Errorf("unrecognized command: %v", input)
	}

	return handler, nil

}

func shiftArgs(args []string) (string, []string) {

	base := strings.ToLower(path.Base(args[0]))

	if base == "scall" {
		return args[1], args[2:]
	}

	return base, args[1:]

}

func init() {
	logger = logging.NewLogger("scall/scall:", logging.Lshortfile|logging.LstdFlags)
}

func Main(args []string) {

	head, tail := shiftArgs(args)

	if len(tail) < 1 {
		logger.Fatal("scall usage")
	}

	if handler, err := parseHandler(head); err != nil {
		logger.Fatal(err)
	} else {

		err := handler(tail)

		if err != nil {
			logger.Fatal(err)
		}
	}

}
