package syscall

import (
	"errors"
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"golang.org/x/sys/unix"
	"os"
	"syscall"
)

type Process struct {
	pid int
}

type ExecVE struct {
	Argv []string `json:"argv"`
	Envp []string `json:"envp"`
}

func (execution ExecVE) Exec() error {

	logger.Printf(logging.DebugLevel,
		"argv[0]: %v", execution.Argv[0],
	)

	logger.Printf(logging.TraceLevel,
		"ExecVE: %v", execution,
	)

	err := unix.Exec(execution.Argv[0], execution.Argv, execution.Envp)

	return err

}

func Transfer(execution ExecVE) {

	err := execution.Exec()

	if err != nil {
		logger.Fatal(err)
	}

}

func (execution ExecVE) ForkExec() (Process, error) {
	return execution.forkExec()
}

func (execution ExecVE) ForkExecWait() (int, error) {

	proc, err := execution.forkExec()

	if err != nil {
		return -1, err
	}

	return proc.Wait()

}

func (proc Process) Wait() (int, error) {

	status, err := waitPid(proc.pid)

	if err != nil {
		return -1, err
	}

	if wIfExited(status) {
		return wExitStatus(status), nil
	}

	return -1, errors.New("process terminated unexpectedly")

}

func (execution ExecVE) forkExec() (Process, error) {

	attr := syscall.ProcAttr{
		Dir: "",
		Env: execution.Envp,
		Files: []uintptr{
			os.Stdin.Fd(),
			os.Stdout.Fd(),
			os.Stderr.Fd(),
		},
		Sys: nil,
	}

	logger.Printf(logging.DebugLevel,
		"argv[0]: %v", execution.Argv[0],
	)

	logger.Printf(logging.TraceLevel,
		"ForkExec: %v", execution,
	)

	pid, err := syscall.ForkExec(execution.Argv[0], execution.Argv, &attr)

	if err != nil {
		return Process{-1}, err
	}

	return Process{pid}, nil

}
