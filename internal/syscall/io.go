package syscall

import (
	"golang.org/x/sys/unix"
)

func Open(path string, mode int, perm uint32) (int, error) {
	return unix.Open(path, mode, perm)
}

func Close(fd int) error {
	return unix.Close(fd)
}

func Read(fd int, size uint16) ([]byte, error) {
	buf := make([]byte, size)
	_, err := unix.Read(fd, buf)
	return buf, err
}

func Write(fd int, buf []byte) error {
	_, err := unix.Write(fd, buf)
	return err
}
