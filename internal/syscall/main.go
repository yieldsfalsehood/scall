package syscall

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"golang.org/x/sys/unix"
)

var logger logging.Logger

const O_NOCTTY = unix.O_NOCTTY

func init() {
	logger = logging.NewLogger("scall/syscall:", logging.Lshortfile|logging.LstdFlags)
}
