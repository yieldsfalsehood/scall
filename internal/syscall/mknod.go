package syscall

import (
	"gitlab.com/yieldsfalsehood/scall/internal/logging"
	"golang.org/x/sys/unix"
)

func Mknod(path string, mode uint32, dev int) error {
	logger.Printf(logging.DebugLevel, "mknod path %v, mode %v, dev %v", path, mode, dev)
	return unix.Mknod(path, mode, dev)
}
