package syscall

import (
	"syscall"
)

func Pipe() ([]int, error) {
	fds := make([]int, 2)
	return fds, syscall.Pipe(fds)
}
