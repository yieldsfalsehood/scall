package syscall

import (
	"golang.org/x/sys/unix"
)

type Redirection struct {
	OldFd     int
	NewFd     int
	Execution *ExecVE
}

func (redirection *Redirection) Exec() error {

	err := unix.Dup2(redirection.OldFd, redirection.NewFd)

	if err != nil {
		return err
	}

	return redirection.Execution.Exec()

}
