package syscall

import (
	"fmt"
	"golang.org/x/sys/unix"
)

type Stat_t unix.Stat_t

func S_ISBLK(mode uint32) bool {
	return ((mode & unix.S_IFMT) == unix.S_IFBLK)
}

func S_ISCHR(mode uint32) bool {
	return ((mode & unix.S_IFMT) == unix.S_IFCHR)
}

func Stat(path string) (*Stat_t, error) {

	var stat unix.Stat_t

	err := unix.Stat(path, &stat)

	if err != nil {
		return nil, fmt.Errorf("could not stat: %v", err)
	}

	result := Stat_t(stat)

	return &result, nil

}
