package syscall

import (
	"golang.org/x/sys/unix"
)

func wExitStatus(status unix.WaitStatus) int {
	return (int(status) & 0xff00) >> 8
}

func wTermSig(status unix.WaitStatus) int {
	return int(status) & 0x7f
}

func wIfExited(status unix.WaitStatus) bool {
	return wTermSig(status) == 0
}

func waitPid(pid int) (unix.WaitStatus, error) {

	var status unix.WaitStatus

	_, err := unix.Wait4(pid, &status, 0, nil)

	return status, err

}
