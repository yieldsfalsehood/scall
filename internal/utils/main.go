package utils

import (
	"gitlab.com/yieldsfalsehood/scall/internal/cmdlang"
	"gitlab.com/yieldsfalsehood/scall/internal/syscall"
	"os"
)

func ParseExecution(args []string) (*syscall.ExecVE, error) {

	argv, err := cmdlang.Parse1(args)

	if err != nil {
		return nil, err
	}

	return &syscall.ExecVE{
		Argv: argv,
		Envp: os.Environ(),
	}, nil

}
