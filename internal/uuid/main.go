package uuid

import (
	"encoding/hex"
)

type UUID struct {
	Bytes []byte
}

func FromBytes(bytes []byte) (UUID, error) {
	return UUID{bytes}, nil
}

func (uuid UUID) String() string {
	return hex.EncodeToString(uuid.Bytes)
}
