#!/bin/sh

set -eu

bdist="$1"
version="$2"

# url of where the bdist was uploaded
url="$3"

release-cli create \
            --name "Release $version" \
            --tag-name "$version" \
            --assets-link "{
                \"name\": \"${bdist}\",
                \"url\": \"${url}\",
                \"filepath\": \"/${bdist}\"
            }"
