#!/bin/sh

set -euo pipefail

docker run -it --rm scall \
       sequence \
       - \
       /bin/echo -n hello \
       - \
       /bin/echo -n world \
       - \
    | grep -q '^helloworld$'
