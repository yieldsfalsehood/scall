#!/bin/bash

set -euo pipefail

./scall dup2 open read scripts/check.d/token fd 0 + /bin/cat + \
    | md5sum \
    | cut -d" " -f1 \
    | grep -q "^7b52fdebcff46a4ed8c2052508bf256b$"

