#!/bin/bash

set -euo pipefail

# this is a weird kinda check - both processes should see `/proc/self`
# resolve to different things, so if all went well we should get two
# unique lines. that doesn't mean those lines aren't junk or such, but
# it does hint that something's right.
./scall fork \
        - \
        /usr/bin/ls -l /proc/self \
        - \
        /usr/bin/ls -l /proc/self \
        - \
    | sort -u \
    | wc -l \
    | grep -q '^2$'
