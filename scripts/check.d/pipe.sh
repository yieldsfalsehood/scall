#!/bin/bash

set -euo pipefail

# {0, 1, 2}, +2 for the pipe, +1 for ls reading the directory
./scall pipe - /usr/bin/ls /proc/self/fd - | wc -l | grep -q '^6$'
