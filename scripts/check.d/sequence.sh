#!/bin/bash

set -euo pipefail

./scall sequence -- /bin/true -- /bin/true --

./scall \
    sequence \
    - \
    /bin/echo -n hello \
    - \
    /bin/echo -n world \
    - \
    | grep -q '^helloworld$'
